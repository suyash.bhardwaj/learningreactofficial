interface AnalysisObj {
    periodLength: number;
    trainingDays: number;
    success: Boolean;
    rating: number;
    ratingDescription: string;
    target: number;
    average: number;
}

interface ExReqObj {
    daily_exercises: number[],
    target: number
  }

interface ErrorObj {
    error: String
}

type RespObj = AnalysisObj | ErrorObj;

export const calculateExercises = (feedinfo: ExReqObj) => {
    const { daily_exercises, target }: ExReqObj = feedinfo;
    let retObj: RespObj = { error: "none" };
    try{
        if((daily_exercises.find(log => isNaN(Number(log)))) || isNaN(target)){
            retObj = { error: 'malformatted parameters' }
            throw new Error('improper feed of the daily exercise hours!');
            }
        else retObj = {
            periodLength: daily_exercises.length,
            trainingDays: daily_exercises.filter(exhr => exhr!=0 ).length,
            success: (daily_exercises.reduce((a,b)=>a+b,0)/daily_exercises.length) >= target,
            rating: 2,
            ratingDescription: 'not too bad but could be better',
            target: target,
            average: (daily_exercises.reduce((a,b)=>a+b,0)/daily_exercises.length)
            }
    }catch(error: unknown) {
        let errorMessage = 'Something bad happened.';
        if (error instanceof Error) {
        errorMessage += ' Error: ' + error.message;
            }
        console.log(errorMessage);
        }
    return retObj;
    }

const calculationFnEx = (dailyexhours: number[]): AnalysisObj => {
    const target: number = Number(dailyexhours.pop());
    return {
        periodLength: dailyexhours.length,
        trainingDays: dailyexhours.filter(exhr => exhr!=0 ).length,
        success: (dailyexhours.reduce((a,b)=>a+b,0)/dailyexhours.length) >= target,
        rating: 2,
        ratingDescription: 'not too bad but could be better',
        target: target,
        average: (dailyexhours.reduce((a,b)=>a+b,0)/dailyexhours.length)
    }
}

const parseMyArguments = (args: string[]): number[] => {
    args.shift();
    args.shift();
    if(args.find(arg => isNaN(Number(arg))))
    throw new Error('improper feed of the daily exercise hours!');
    else return args.map(arg => Number(arg));
}

export const calculateExercisesFromArgs = () => {
    try {
        const verifiedParams = parseMyArguments(process.argv);
        console.log('this has been returned ', calculationFnEx(verifiedParams));
    }catch(error: unknown) {
        let errorMessage = 'Something bad happened.';
        if (error instanceof Error) {
        errorMessage += ' Error: ' + error.message;
        }
        console.log(errorMessage);
    }
}

// calculateExercisesFromArgs();
// const express = require('express');
// import express from 'express';
import express = require('express');
import { calculator, Operation } from './calculator';
const app = express();

const a : any = /* no clue what the type will be! */

app.get('/ping', (_req, res) => {
  res.send('pong');
});

const PORT = 3003;

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});

app.post('/calculate', (req, res) => {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment 
    const { value1, value2, op } = req.body;
    let value3 : any = 1;
    const val4: any = 45;
    // const result = calculator(value1, value2, op);

    // because we are not beign specific about the type here (allowing 'any'), we need to ensure.. 
    if ( !value1 || isNaN(Number(value1)) ) {    return res.status(400).send({ error: '...'});  }

    const operation = op as Operation;  //used a type assertion (basically, quieting an Eslint rule))
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    const result = calculator(Number(value1), Number(value2), operation);
    res.send({ result });
});
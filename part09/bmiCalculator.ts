interface bmifeed {
    ht: number,
    wt: number
}

const parseArgsForBMI = (h: number, w: number): bmifeed => {
    if(isNaN(h) || isNaN(w))
        throw new Error('improper feed for calculating the BMI!');
    else return { ht:h, wt:w };
}

export const calculateBmi = (h: number, w: number): String => {
    let certificate = "unset";
    try {
        const {ht, wt} = parseArgsForBMI(h, w);
        const bmi: number = wt/(0.01*ht*ht);
        console.log('calculated BMI is ', bmi);
        certificate = bmi<1.8 ? "under weight" : "healthy weight";
    }catch(error: unknown) {
        let errorMessage = 'Something bad happened.';
        if (error instanceof Error) {
          errorMessage += ' Error: ' + error.message;
        }
        console.log(errorMessage);
      }
      return String(certificate);
    }
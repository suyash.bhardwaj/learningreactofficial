import express = require('express');
import { calculateBmi } from './bmiCalculator';
import { calculator } from './calculator';
import { calculateExercises } from './exerciseCalculator';
const app = express();
const PORT = 3003;
// type queryparams = String | Number;

interface ExReqObj {
  daily_exercises: number[],
  target: number
}

app.use(express.json());

// let lucky: number;
// lucky = '23';

app.get('/hello', (_req, res) => {
    res.send('Hello FullStack');
  });

app.get('/bmi', (_req, res) => {
    console.log(_req.query);
    const responseMsg = calculateBmi(Number(_req.query.weight),Number(_req.query.height));
    responseMsg ==="unset"? 
        res.json({error: "malformatted parameters"}).status(404)
        : res.send(responseMsg)
  });

app.post('/calculate', (req, res) => {
    const { value1, value2, op } = req.body;
    const pratiuttar = calculator(value1, value2, op);
    res.send({ pratiuttar });
})

app.post('/exercises', (req, res) => {
  const feedinfo: ExReqObj= req.body;
  const result = calculateExercises(feedinfo);
  res.send({ result });
})

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
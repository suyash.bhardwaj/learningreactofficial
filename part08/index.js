const { v1: uuid } = require('uuid')
const { GraphQLError } = require('graphql')

const { ApolloServer } = require('@apollo/server')
const { startStandaloneServer } = require('@apollo/server/standalone')

// THE DATA: in json form
let authors = [
  {
    name: 'Robert Martin',
    id: "afa51ab0-344d-11e9-a414-719c6709cf3e",
    born: 1952,
  },
  {
    name: 'Martin Fowler',
    id: "afa5b6f0-344d-11e9-a414-719c6709cf3e",
    born: 1963
  },
  {
    name: 'Fyodor Dostoevsky',
    id: "afa5b6f1-344d-11e9-a414-719c6709cf3e",
    born: 1821
  },
  { 
    name: 'Joshua Kerievsky', // birthyear not known
    id: "afa5b6f2-344d-11e9-a414-719c6709cf3e",
  },
  { 
    name: 'Sandi Metz', // birthyear not known
    id: "afa5b6f3-344d-11e9-a414-719c6709cf3e",
  },
];

/*
 * Suomi:
 * Saattaisi olla järkevämpää assosioida kirja ja sen tekijä tallettamalla kirjan yhteyteen tekijän nimen sijaan tekijän id
 * Yksinkertaisuuden vuoksi tallennamme kuitenkin kirjan yhteyteen tekijän nimen
 *
 * English:
 * It might make more sense to associate a book with its author by storing the author's id in the context of the book instead of the author's name
 * However, for simplicity, we will store the author's name in connection with the book
 *
 * Spanish:
 * Podría tener más sentido asociar un libro con su autor almacenando la id del autor en el contexto del libro en lugar del nombre del autor
 * Sin embargo, por simplicidad, almacenaremos el nombre del autor en conección con el libro
*/

let books = [
  {
    title: 'Clean Code',
    published: 2008,
    author: 'Robert Martin',
    id: "afa5b6f4-344d-11e9-a414-719c6709cf3e",
    genres: ['refactoring']
  },
  {
    title: 'Agile software development',
    published: 2002,
    author: 'Robert Martin',
    id: "afa5b6f5-344d-11e9-a414-719c6709cf3e",
    genres: ['agile', 'patterns', 'design']
  },
  {
    title: 'Refactoring, edition 2',
    published: 2018,
    author: 'Martin Fowler',
    id: "afa5de00-344d-11e9-a414-719c6709cf3e",
    genres: ['refactoring']
  },
  {
    title: 'Refactoring to patterns',
    published: 2008,
    author: 'Joshua Kerievsky',
    id: "afa5de01-344d-11e9-a414-719c6709cf3e",
    genres: ['refactoring', 'patterns']
  },  
  {
    title: 'Practical Object-Oriented Design, An Agile Primer Using Ruby',
    published: 2012,
    author: 'Sandi Metz',
    id: "afa5de02-344d-11e9-a414-719c6709cf3e",
    genres: ['refactoring', 'design']
  },
  {
    title: 'Crime and punishment',
    published: 1866,
    author: 'Fyodor Dostoevsky',
    id: "afa5de03-344d-11e9-a414-719c6709cf3e",
    genres: ['classic', 'crime']
  },
  {
    title: 'The Demon ',
    published: 1872,
    author: 'Fyodor Dostoevsky',
    id: "afa5de04-344d-11e9-a414-719c6709cf3e",
    genres: ['classic', 'revolution']
  },
];

/*
  you can remove the placeholder query once your first one has been implemented 
*/

// THE VARIOUS SCHEMAS: OF DATA, OF QUERY, OF QUERY WITH ENUMS, OF MUTATIONS (ADD, UPDATE): how the data is formatted, what all can be inquired, what all updations in data can be made
const typeDefs = `
    type Authors {
        name: String!
        id: String!
        born: String
    }
    
    type Books {
        title: String!
        published: Int!
        author: String!
        id: String!
        genres: [String!]!
    }

    type Query {
        authorCount: Int!
        bookCount: Int!
        allAuthors: [Authors!]!
        allBooks(author:String, genre:String): [Books!]!
    }

    type Mutation {
      addBook(
        title: String!
        published: Int!
        author: String
        genres: [String!]!
      ): Books

      editAuthor(
        name: String!
        setBornTo: Int!
      ): Authors
    }
`;

//  EXECUTION PLANS: how are we gonna go about each query, mutation, data accesses specified in the schemas
const resolvers = {
  Query: {
    authorCount: () => authors.length,
    bookCount: () => books.length,
    allAuthors: (root, args) => authors,
    // allBooks: (root, args) => books.filter(bk => bk.author == args.author),
    // allBooks: (root, args) => books.filter(bk => bk.genres.find(gr => gr === args.genre)),
    allBooks: (root, args) => 
      args.genre && args.author
        ? books.filter(bk => bk.author===args.author).filter(bk => bk.genres.find(gr=>gr===args.genre))
        : args.genre 
          ? books.filter(bk => bk.genres.find(gr => gr===args.genre))
          : args.author
            ? args.filter(bk => bk.author == args.author)
              : books
  },
  Mutation: {
    addBook: (root, args) => {
      if(!authors.find(author => author.name === args.author)) {
        const newAuthor = { id:uuid() ,name: args.author, born: null}
        authors = authors.concat(newAuthor);
        console.log('a new author has been added!');
      }
      
      if (books.find(bk => bk.title === args.title)) {
        throw new GraphQLError('Bhai, Book title must be unique!', { extensions:{code: 'BAD_USER_INPUT', invalidArgs: args.name} })
        }
      const book = { ...args, id: uuid() }
      books = books.concat(book)
      // console.log("books is being returned as ", books);
      return books;
    },

    editAuthor: (root, args) => {
      if(!args.name) return({editAuthor: null})
      const author2bupdated = authors.find(author => author.name === args.name)
      if(!author2bupdated) return null;
      const updatedAuthor = { ...author2bupdated, born: args.setBornTo }
      // delete updatedAuthor.setBornTo;
      
      authors = authors.map(author => author.name === args.name ? updatedAuthor : author)
      // console.log("updatedAuthor is ", updatedAuthor);
      // console.log("author2bupdated is ", author2bupdated);
      // console.log("The new author array is ", authors);
      return updatedAuthor;
    }
  }
}

const server = new ApolloServer({
  typeDefs,
  resolvers,
})

startStandaloneServer(server, {
  listen: { port: 4000 },
}).then(({ url }) => {
  console.log(`Server ready at ${url}`)
})
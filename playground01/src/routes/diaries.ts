// diary data पर सारॆ लॆन दॆन करनॆ कॆ लिय सारॆ routes यहां हॆं
import express from 'express';
import diaryService from '../services/diaryService'; //सभि data accessor methods dairyService provide करायगा

const diaryRouter = express.Router();

diaryRouter.get('/', (_req, res) => {
  res.send(diaryService.getNonSensitiveEntries());
});

diaryRouter.get('/:id', (req, res) => {
  const diary = diaryService.findById(Number(req.params.id));

  if (diary) {
    res.send(diary);
  } else {
    res.sendStatus(404);
  }
});

diaryRouter.post('/', (req, res) => {
  const { date, weather, visibility, comment } = req.body;
  const newDiaryEntry = diaryService.addDiary({
    date,
    weather,
    visibility,
    comment,
  });
  res.json(newDiaryEntry);
});

export default diaryRouter;
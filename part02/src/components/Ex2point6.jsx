//Features sulution for exercise 2.6
import React from 'react'
import { useState } from 'react'

const Ex2point6 = () => {
    const [persons, setPersons] = useState( [{ name: 'Arto Hellas', no: '9783463476' }, { name: 'Hari Sadu', no: '7635294353' }] ) 
    const [newName, setNewName] = useState('')

    const handleSubmit = (evt) => {
        const enteredName = evt.target.nameOfContact.value;
        const enteredNo = evt.target.numberOfContact.value;
        evt.preventDefault();
        // console.log("has been submitted is ",evt.target.elements.nameOfContact);
        // console.dir(evt.target.elements.nameOfContact);
        // console.log("target.value", evt.target.value);
        // console.log("target.value", evt.target.nameOfContact.value);
        persons.find(person => person.name == enteredName) ? 
            alert(`${enteredName} is already there in the phonebook`) : setPersons([...persons, { name: enteredName, no: enteredNo}]);
    }

    return (
        <div>
            <div>debug: {newName}</div>
            <h2>Phonebook</h2>
            <form onSubmit={handleSubmit}>
                <div> name: <input name = "nameOfContact"/> number: <input name = "numberOfContact"/></div>
                <div> <button type="submit">add</button> </div>
            </form>
            <h2>Numbers</h2>
            {persons.map(person=> <li key={person.name}>{person.name} | {person.no}</li>)}
        </div>
  )
}

export default Ex2point6
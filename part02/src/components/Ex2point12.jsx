import axios from 'axios'
import { useEffect, useState } from 'react'

const Ex2point12 = () => {
  const [persons, setPersons] = useState([])

  const handleSubmit = (evt) => {
    evt.preventDefault();
    const enteredName = evt.target.nameOfContact.value;
    const enteredNo = evt.target.numberOfContact.value;
    persons.find(person => person.name == enteredName) ? 
            alert(`${enteredName} is already there in the phonebook`) 
            : setPersons([...persons, { name: enteredName, no: enteredNo}]);
            // : axios.post()
  }

  useEffect(()=> {
    axios.get('http://localhost:3001/persons').then(response => {
        const persons = response.data;
        console.log(persons);
        setPersons(persons);
  })},[]);
  
  return (
    <div>
        <h2>Phonebook</h2>
            <form onSubmit={handleSubmit}>
                <div> name: <input name = "nameOfContact"/> number: <input name = "numberOfContact"/></div>
                <div> <button type="submit">add</button> </div>
            </form>
        <h2>Numbers</h2>    
        <ul> {persons.map((person,i) => <li key={i} >{person.name} | {person.number}</li>)} </ul>
    </div>
  )
}

export default Ex2point12

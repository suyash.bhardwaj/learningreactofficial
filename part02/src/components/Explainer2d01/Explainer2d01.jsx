import React from 'react'
import { useState, useEffect } from 'react'
import axios from 'axios'
// import noteService from './services/notes'

const Note = ({note, toggleImportance}) => { 
    const label = note.important ? 'make not important' : 'make important'
    return(
        <li>
            {note.content}
            <button onClick={toggleImportance}>{label}</button>
        </li>
        ) 
    }

const Explainer2d01 = () => {
    // const [notes, setNotes] = useState(props.notes)
    const [notes, setNotes] = useState([])
    const [newNote, setNewNote] = useState(    'a new note...'  ) 
    const [showAll, setShowAll] = useState(true)

    // const notesToShow = showAll ? notes : notes.filter(note => note.important === true)
    
    const handleNoteChange = (event) => {
        console.log(event.target.value);
        setNewNote(event.target.value)
    }

    const addNote = (event) => {
        event.preventDefault();
        const noteObject = {
            content: newNote,
            important: Math.random() <0.5,
        }
        axios.post('http://localhost:3001/notes', noteObject).then(response => {
            console.log(response)
            // setNotes(notes.concat(response.data))
            // setNewNote('')
        });
        // noteService.create(noteObject).then(returnedNote => { setNotes(notes.concat(returnedNote)); setNewNote('')})
    }

    const toggleImportanceOf = (id) => { 
    //     const url = `http://localhost:3001/notes/${id}`
    //     const note = notes.find(n => n.id === id)
    //     const changedNote = { ...note, important: !note.important }
    //     axios.put(url, changedNote).then(response => {
    //         setNotes(notes.map(n => n.id !== id ? n : response.data))
    //         })
        // noteService.update(id, changedNote)
        //     .then(returnedNote => { setNotes(notes.map(note => note.id !== id ? note : returnedNote)) })
        //     .catch(error => { alert(`the note '${note.content}' was already deleted from server` ); setNotes(notes.filter(n => n.id !== id))})
        }

    // useEffect(() => {noteService.getAll().then(initialNotes => { setNotes(initialNotes) })}, [])
    return (
    <div>
        <h1>Notes</h1>
        <div>
            <button onClick={() => setShowAll(!showAll)}>
                show {showAll ? 'important' : 'all' }
            </button>
        </div>

        {/* <ul>
            {notesToShow.map(note => 
            <Note key={note.id} note={note} toggleImportance={() => toggleImportanceOf(note.id)} />
            )}
        </ul> */}

        <form onSubmit={addNote}>
            <input value={newNote} onChange={handleNoteChange}/>
            <button type="submit">save</button>
        </form>   
    </div>
  )
}

export default Explainer2d01

import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
// import axios from 'axios'

// const promise = axios.get('http://localhost:3001/notes')
// console.log(promise)
// promise.then(res => console.log(res));

// axios.get('http://localhost:3001/notes').then(response => {
//   const notes = response.data
//   console.log(notes)
// })

// axios
//   .get('http://localhost:3001/notes')
//   .then(response => {
//     const notes = response.data
//     console.log(notes)
//   })

// const promise2 = axios.get('http://localhost:3001/foobar')
// console.log(promise2)
// promise2.then(res => console.log(res));

// axios.get('http://localhost:3001/notes').then(response => {
//   const notes = response.data;
//   ReactDOM.createRoot(document.getElementById('root')).render(
//     <React.StrictMode> <App notes = {notes}/> </React.StrictMode>)})

  ReactDOM.createRoot(document.getElementById('root')).render(<React.StrictMode> <App/> </React.StrictMode>)
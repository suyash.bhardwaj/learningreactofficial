import ReactDOM from 'react-dom/client'
import App from './App'

 import { ApolloClient, InMemoryCache, gql, ApolloProvider } from '@apollo/client'

// instantiation of an object that gives us a method to fire a query 
const client = new ApolloClient({
  uri: 'http://localhost:4000',
  cache: new InMemoryCache(),
})

// const query = gql`
//   query {
//     allPersons  {
//       name,
//       phone,
//       address {
//         street,
//         city
//       }
//       id
//     }
//   }
// `

// client.query({ query })
//   .then((response) => {
//     console.log(response.data)
//   })

ReactDOM.createRoot(document.getElementById('root')).render(<ApolloProvider client={client}> <App/> </ApolloProvider>)
// ReactDOM.createRoot(document.getElementById('root')).render(<App/>);
/*Sections covered in this program*/
// Complex State
import React from 'react'
import { useState } from "react"

const Explainer1d01 = () => {
    // const [left, setLeft] = useState(0)
    // const [right, setRight] = useState(0)
    const [clicks, setClicks] = useState( {left: 0, right: 0} );
    
    // const handleLeftClick = () => {
    //   const newClicks = { 
    //     // right: clicks.right 
    //     ...clicks,
    //     left: clicks.left + 1,  
    //   }
    //   setClicks(newClicks)
    // }

    const handleLeftClick = () => setClicks({ ...clicks, left: clicks.left + 1 })

    // const handleRightClick = () => {
    //   const newClicks = { 
    //     // left: clicks.left, 
    //     ...clicks, 
    //     right: clicks.right + 1 
    //   }
    //   setClicks(newClicks)
    // }

    const handleRightClick = () => setClicks({ ...clicks, right: clicks.right + 1 })

    return (
        <div>
            {/* {left} */}
            {clicks.left}
            {/* <button onClick={() => setLeft(left + 1)}>left</button> */}
            <button onClick={handleLeftClick}>left</button>
            {/* <button onClick={() => setRight(right + 1)}>right</button> */}
            <button onClick={handleRightClick}>right</button>
            {/* {right} */}
            {clicks.right}
        </div>
    )
    }

export default Explainer1d01